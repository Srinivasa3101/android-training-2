package com.example.training2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ListView listView;

    private RecyclerView rcList;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;

//    int[] images = {
//            R.drawable.asia,
//            R.drawable.person,
//            R.drawable.student,
//            R.drawable.urban
//    };
//
//    String[] Names = {
//            "JOHN",
//            "ALEX",
//            "Peter",
//            "MELON"
//    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ArrayList<list> lists = new ArrayList<>();
        lists.add((new list(R.drawable.asia, "JOHN")));
        lists.add((new list(R.drawable.person, "ALEX")));
        lists.add((new list(R.drawable.student, "PETER")));
        lists.add((new list(R.drawable.urban, "MELON")));




        rcList = findViewById(R.id.rcList);
        rcList.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        adapter = new listAdapter(lists);
//        listView = findViewById(R.id.list);
//        CusList cusList = new CusList();



        rcList.setLayoutManager(layoutManager);

        // rcList.addItemDecoration(new DividerItemDecoration(rcList.getContext(), DividerItemDecoration.VERTICAL));


        rcList.setAdapter(adapter);
    }

//    class CusList extends BaseAdapter{
//
//        @Override
//        public int getCount() {
//            return images.length;
//        }
//
//        @Override
//        public Object getItem(int i) {
//            return null;
//        }
//
//        @Override
//        public long getItemId(int i) {
//            return 0;
//        }
//
//        @Override
//        public View getView(int i, View view, ViewGroup viewGroup) {
//            View view1 = getLayoutInflater().inflate(R.layout.list, null);
//
//            ImageView imageView = view1.findViewById(R.id.imageView);
//            TextView textView = view1.findViewById(R.id.textView);
//
//            imageView.setImageResource(images[i]);
//            textView.setText(Names[i]);
//
//            return view1;
//        }
//    }

}