package com.example.training2;

public class list {
    private int images;
    private String names;

    public  list(int image, String name){
        images = image;
        names = name;
    }

    public  int getImages(){
        return images;
    }

    public  String getNames(){
        return names;
    }
}
