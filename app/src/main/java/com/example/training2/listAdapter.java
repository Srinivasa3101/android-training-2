package com.example.training2;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class listAdapter extends RecyclerView.Adapter<listAdapter.listViewHolder> {

    private ArrayList<list> mlist;

    public  static  class  listViewHolder extends  RecyclerView.ViewHolder{

        public ImageView imageView;
        public TextView textView;

        public listViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageView);
            textView = itemView.findViewById(R.id.textView);
        }
    }

    public  listAdapter(ArrayList<list> lists){
        mlist = lists;
    }

    @NonNull
    @Override
    public listViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list, parent, false);
        listViewHolder listViewHolder = new listViewHolder(view);
        return  listViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull listViewHolder holder, int position) {
        list currentItem = mlist.get(position);

        holder.imageView.setImageResource(currentItem.getImages());
        holder.textView.setText(currentItem.getNames());
    }

    @Override
    public int getItemCount() {
        return mlist.size();
    }
}
